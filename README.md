# Deploy to EKS Cluster from Jenkins Pipeline 2024

### Pre-Requisites

AWS

eks command line

git

Digital Ocean droplet

Jenkins

#### Project Outline

In this project we will deploy to our eks cluster from a Jenkins pipeline

Using the cluster from the below project

[Link to EKS Setup](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024)

#### Projects used

[cd-deploy-to-eks-cluster-from-jenkins-pipeline](https://gitlab.com/FM1995/cd-deploy-to-eks-cluster-from-jenkins-pipeline/-/blob/main)

#### Getting started

Checking we don’t have any pods

```
kubectl get pod
```

![Image1](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image1.png?ref_type=heads)

So now we are ready to start deploying applications inside
So there are a few things we need to configure

•	Install kubectl command line tool inside Jenkins container
So that we can execute kubectl commands in the pipeleline

•	Install aws-iam-authentication
So that we can authenticate with AWS and deploy to the cluster

•	Create kubeconfig file to connect to EKS cluster

We will go in to the Jenkins container to authenticate the connection with eks cluster and AWS account in AWS, and is an alternative to AWS credentials

This is basically the same as updating the .kubec/config file with the API server endpoint 

And is the same as the below, where it fetches the necessary information about the specified EKS cluster, including the cluster endpoint and the authentication credentials and updated the kubeconfig on our local machine with this info, from there we can run kubectl commands

Below is an example:

To create a file to connect to the eks cluster

```
aws eks update-kubeconfig --name eks-cluster-test
```

![Image2](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image2.png?ref_type=heads)


![Image3](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image3.png?ref_type=heads)

And can now cat it to see the authentication certificate

![Image4](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image4.png?ref_type=heads)

And can also see the cluster info

```
kubectl cluster info
```

![Image5](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image5.png?ref_type=heads)


Usually aws-iam-authenticator is installed with eksctl

But for demonstration purposes we can install using many tools, I will use choco, below is the command

```
choco install -y aws-iam-authenticator
```

![Image6](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image6.png?ref_type=heads)

Can see it is enabled

![Image7](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image7.png?ref_type=heads)


Now referring to the steps we need to configure we need to exactly the same in the Jenkins container so that Jenkins can do the same

Now referring to the steps we need to configure we need exactly the same in the Jenkins container so that Jenkins can do the same
Another step is

•	Adding AWS credentials on Jenkins for AWS account authentication
Above is done so that we can connect to AWS and the eks cluster from the pipeline

•	Adjust the Jenkinsfile to configure EKS cluster deployment

Lets get started

Lets begin by installing kubectl on Jenkins server

Ssh’ing into the digital ocean droplet running the Jenkins server

Once ssh’d into the server can check the container processes

![Image8](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image8.png?ref_type=heads)

Now let’s enter the container as a root user as the Jenkins user does not have admin permissions

```
docker exec -u 0 -it a980f6f5e36e bash
```

Can use the below links to install kubectl inside the container

https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

Can use the below curl command

```
curl -LO https://dl.k8s.io/release/v1.24.7/bin/linux/amd64/kubectl
```

![Image9](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image9.png?ref_type=heads)

Next steps include making the file executable and moving the file

```
chmod +x ./kubectl
```

And then moving the file to the bin directory and renaming it to ‘kubectl’

```
mv ./kubectl /usr/local/bin/kubectl
```

![Image10](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image10.png?ref_type=heads)

And can now see the version

![Image11](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image11.png?ref_type=heads)

Next step is to install aws-iam-authenticator

Using the below link

https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html

Can utilise the below curl command

```
curl -Lo aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.5.9/aws-iam-authenticator_0.5.9_linux_amd64
```

![Image12](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image12.png?ref_type=heads)

Can also run the below to make it executable

```
chmod +x aws-iam-authenticator
```

And then move it to the bin directory similar to the kubectl previously

```
mv ./aws-iam-authenticator /usr/local/bin
```

![Image13](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image13.png?ref_type=heads)

Can then run the below to check the installation

```
aws-iam-authenticator help
```

And can see it is now installed

![Image14](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image14.png?ref_type=heads)

Now lets create kubeconfig file for Jenkins, this is because jenkins container does not have any editor like vim or nano

So we will create it on the digital ocean host and then simply copy it

So on the host lets do the below

```
vim config
```

Using the below link

https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html#create-kubeconfig-automatically

Can copy the contents

![Image15](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image15.png?ref_type=heads)

Can then make changes to cluster name, in the above we inserted our cluster name which is ‘demo-cluster’

Second value is the API Server endpoint which we can get from the cluster

![Image16](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image16.png?ref_type=heads)

And the final is the certificate-authority-data

Can get it from the AWS UI or locally

To get it locally can use the below

```
cat ~/.kube/config
```

Can then copy the contents and insert into the config file

Finalised

![Image17](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image17.png?ref_type=heads)

Can now save it

Can login to the container and make the directory

![Image18](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image18.png?ref_type=heads)

Can then exit Jenkins container and go back to the server that is running jenkins container and copy config file to .kube directory in home directory of jenkins container

```
docker cp config  a980f6f5e36e:/var/jenkins_home/.kube
```

![Image19](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image19.png?ref_type=heads)

Checking to see file and can see the below file

![Image20](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image20.png?ref_type=heads)

Next we will create AWS credentials for the user

Can create the credentials

Using the details in ~/.aws/config

Can fill in the details for access and secret access key

![Image21](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image21.png?ref_type=heads)


Can configure the same secret for the texts

![Image22](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image22.png?ref_type=heads)


Now lets configure Jenkinsfile to deploy to EKS

Taking a basic Jenkinsfie

![Image23](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image23.png?ref_type=heads)

Need to reconfigure the Jenkinsfile

We will include a sh command to create a deployment using nginx image

And also export the environment variables which are the AWS Access and secret keys which will be used a credentials to connect to AWS and the EKS cluster

Completed Jenkinsfile

![Image24](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image24.png?ref_type=heads)

Now ready to push to repository

![Image25](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image25.png?ref_type=heads)

And is now ready for build and can see success

![Image26](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image26.png?ref_type=heads)

Now if we go back to git terminal can see nginx is deployed

```
kubectl get pod
```
![Image27](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image27.png?ref_type=heads)

Below is the Jenkinsfile used

[Finalised Jenkinsfile](https://gitlab.com/FM1995/cd-deploy-to-eks-cluster-from-jenkins-pipeline/-/blob/main/Jenkinsfile?ref_type=heads)














